# coding=utf-8
BROKER_URL = 'amqp://dongwm:123456@localhost:5672/web_develop'  # 使用RabbitMQ作物消息代理
CELERY_RESULT_BACKEND = 'redis://localhost:6379/0'  # 任务结果存放redis
CELERY_TASK_SERIALIZER = 'msgpack'  # 任务序列化和反序列化使用msgpack方法
CELERY_RESULT_SERIALIZER = 'json'   # 读取任务json
CELERY_TASK_RESULT_EXPIRES = 60 * 60 * 24   # 任务国企时间
CELERY_ACCEPT_CONTENT = ['json', 'msgpack'] # 指定接收的内容类型
